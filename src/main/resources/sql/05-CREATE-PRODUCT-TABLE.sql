CREATE TABLE IF NOT EXISTS PRODUCT
(
    ID                  SERIAL PRIMARY KEY,
    PRODUCT_NAME        VARCHAR(1000) NOT NULL,
    ACTIVE              BOOLEAN DEFAULT TRUE
);