package com.jigarnaik.finman.repository;

import com.jigarnaik.finman.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAllByActiveOrderByProductName(Boolean active);
}
