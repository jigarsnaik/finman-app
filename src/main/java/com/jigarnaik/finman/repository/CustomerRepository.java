package com.jigarnaik.finman.repository;

import com.jigarnaik.finman.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByActiveOrderByCustomerNameAsc(Boolean active);

    List<Customer> findByCustomerNameAndActive(String customerName, Boolean active);
}
