package com.jigarnaik.finman.repository;

import com.jigarnaik.finman.domain.Address;
import com.jigarnaik.finman.domain.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends CrudRepository<Address, Long> {

    List<Address> findByCustomer(Customer customer);
}
