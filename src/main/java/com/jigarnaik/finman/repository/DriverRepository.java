package com.jigarnaik.finman.repository;

import com.jigarnaik.finman.domain.Driver;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DriverRepository extends CrudRepository<Driver, Long> {
    List<Driver> findAllByActiveOrderByDriverName(Boolean active);
}
