package com.jigarnaik.finman.controller;

import com.jigarnaik.finman.domain.Invoice;
import com.jigarnaik.finman.service.InvoiceService;
import com.jigarnaik.finman.util.PDFUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Objects;

@RestController
@RequestMapping("/invoices")
@CrossOrigin(origins = "*")
@Log4j2
public class InvoiceController {

    private final InvoiceService invoiceService;
    private final ResourceLoader resourceLoader;


    public InvoiceController(InvoiceService invoiceService, ResourceLoader resourceLoader) {
        this.invoiceService = invoiceService;
        this.resourceLoader = resourceLoader;
    }

    @GetMapping("/change")
    public ResponseEntity<String> changes() {
        return ResponseEntity.ok("dummy deployment");
    }

    @PostMapping
    public ResponseEntity<Invoice> createInvoice(@RequestBody Invoice invoice) {
        var savedInvoice = invoiceService.saveOrUpdate(invoice);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedInvoice);
    }

    @GetMapping("/{id}/download")
    public ResponseEntity<byte[]> downloadInvoice(@PathVariable(value = "id") Long id) throws Exception {
        log.info("Request received to download invoice with id {}", id);
        Invoice invoice = invoiceService.findById(id);
        byte[] bytes = resourceLoader.getResource("classpath:Balaji Delivery Challan.pdf").getInputStream().readAllBytes();
        byte[] filledBytes = PDFUtil.fillChallan(bytes, invoice);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Access-Control-Expose-Headers", "*");
        headers.add("Access-Control-Allow-Headers", "*");
        headers.setContentLength(filledBytes.length);
        headers.setContentType(new MediaType("application", "pdf"));
        headers.setCacheControl("no-cache");
        String fileName = "invoice_" + invoice.getInvoiceNo();
        headers.set(HttpHeaders.CONTENT_DISPOSITION, String.format("inline; filename=\" " + fileName + "\""));
        log.info("Success : Sending Response : File Name : {} , length : {}", "invoice-test.pdf", filledBytes.length);
        return new ResponseEntity<>(filledBytes, headers, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Invoice> updateInvoice(@RequestBody Invoice invoice) {
        var updatedInvoice = invoiceService.saveOrUpdate(invoice);
        return ResponseEntity.ok(updatedInvoice);
    }

    @GetMapping
    public ResponseEntity<Page<Invoice>> findAllActive(@RequestParam(name = "royaltyNo", required = false) String royaltyNo,
                                                       @RequestParam(name = "invoiceNo", required = false) String invoiceNo,
                                                       @RequestParam(name = "customerName", required = false) String customerName,
                                                       @RequestParam(name = "purchaseDateFrom", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate purchaseDateFrom,
                                                       @RequestParam(name = "purchaseDateTo", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") LocalDate purchaseDateTo,
                                                       @RequestParam(name = "page", defaultValue = "0") int page,
                                                       @RequestParam(name = "size", defaultValue = "3") int size) {
        var allActiveInvoice = invoiceService.findInvoices(royaltyNo, invoiceNo,customerName, purchaseDateFrom, purchaseDateTo,PageRequest.of(page, size));
        return ResponseEntity.ok(allActiveInvoice);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Invoice> findById(@PathVariable(value = "id") Long id) {
        var invoice = invoiceService.findById(id);
        return ResponseEntity.ok(invoice);
    }

    @GetMapping("/invoiceNo/next")
    public String getNextInvoiceNo() {
        return invoiceService.getNextInvoiceNo();
    }


}
