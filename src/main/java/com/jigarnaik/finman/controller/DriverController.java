package com.jigarnaik.finman.controller;

import com.jigarnaik.finman.domain.Driver;
import com.jigarnaik.finman.service.DriverService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/drivers")
@CrossOrigin(origins = "*")
@Log4j2
public class DriverController {

    private final DriverService driverService;

    public DriverController(DriverService driverService) {
        this.driverService = driverService;
    }

    @PostMapping
    public ResponseEntity<Driver> createProduct(@RequestBody Driver driver) {
        var savedDriver = driverService.saveOrUpdate(driver);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedDriver);
    }

    @GetMapping
    public ResponseEntity<List<Driver>> findAllDrivers() {
        var allActiveDrivers = driverService.findAllActive();
        return ResponseEntity.ok(allActiveDrivers);
    }
}