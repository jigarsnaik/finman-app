package com.jigarnaik.finman.controller;

import com.jigarnaik.finman.domain.Product;
import com.jigarnaik.finman.service.ProductService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
@CrossOrigin(origins = "*")
@Log4j2
public class ProductController {

    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;

    }

    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        var savedProduct = productService.saveOrUpdate(product);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedProduct);
    }

    @GetMapping
    public ResponseEntity<List<Product>> findAllProducts() {
        var allActiveProducts = productService.findAllActive();
        return ResponseEntity.ok(allActiveProducts);
    }
}