package com.jigarnaik.finman.controller;

import com.jigarnaik.finman.domain.Address;
import com.jigarnaik.finman.domain.Customer;
import com.jigarnaik.finman.service.AddressService;
import com.jigarnaik.finman.service.CustomerService;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@CrossOrigin(origins = "*")
@Log4j2
public class CustomerController {

    private final CustomerService customerService;
    private final AddressService addressService;

    public CustomerController(CustomerService customerService, AddressService addressService) {
        this.customerService = customerService;
        this.addressService = addressService;
    }

    @PostMapping
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
        var savedInvoice = customerService.saveOrUpdate(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedInvoice);
    }

    @PutMapping
    public ResponseEntity<Customer> updateCustomer(@RequestBody Customer customer) {
        var updatedInvoice = customerService.saveOrUpdate(customer);
        return ResponseEntity.ok(updatedInvoice);
    }

    @GetMapping
    public ResponseEntity<List<Customer>> findAllCustomers() {
        var activeCustomers = customerService.findAllActiveCustomers();
        return ResponseEntity.ok(activeCustomers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> findById(@PathVariable(value = "id") Long id) throws Exception {
        var customer = customerService.findById(id);
        return ResponseEntity.ok(customer);
    }

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> findByCustomerName(@RequestParam(required = false, name = "customerName") String customerName) {
        var customers = customerService.findByCustomerName(customerName);
        return ResponseEntity.ok(customers);
    }

    @GetMapping("/customers/{customerId}/addresses")
    public ResponseEntity<List<Address>> findAddressByCustomerId(@PathVariable Long customerId) {
        var addresses = addressService.findByCustomer(customerId);
        return ResponseEntity.ok(addresses);
    }

}
