package com.jigarnaik.finman.util;

import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.jigarnaik.finman.domain.Invoice;
import lombok.extern.log4j.Log4j2;

import java.io.ByteArrayOutputStream;
import java.time.format.DateTimeFormatter;

@Log4j2
public class PDFUtil {

    private PDFUtil() {

    }

    public static byte[] fillChallan(byte[] data, Invoice invoice) throws Exception {
        PdfStamper stamper = null;
        PdfReader pdfReader = null;
        ByteArrayOutputStream baos = null;
        try {
            pdfReader = new PdfReader(data);
            baos = new ByteArrayOutputStream();
            stamper = new PdfStamper(pdfReader, baos);
            AcroFields form = stamper.getAcroFields();
            form.setGenerateAppearances(true);
            form.setField("challanNo", invoice.getInvoiceNo());
            form.setField("royaltyNo", invoice.getRoyaltyNo());
            form.setField("purchaseDate", invoice.getPurchaseDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
            form.setField("customerName", invoice.getCustomer().getCustomerName());
            form.setField("address1", invoice.getAddress().getAddress());
            form.setField("address2", (invoice.getAddress().getCity() == null ? "" : invoice.getAddress().getCity())
                    + (invoice.getAddress().getPostalCode() == null ? "" : invoice.getAddress().getPostalCode()));
            form.setField("productName", invoice.getProduct().getProductName());
            form.setField("lengthGrossWt", (invoice.getLengthGrossWt() == null ? "N/A" : String.valueOf(invoice.getLengthGrossWt())));
            form.setField("widthTareWt", (invoice.getWidthTareWt() == null ? "N/A" : String.valueOf(invoice.getWidthTareWt())));
            form.setField("heightNetWt", (invoice.getHeightNetWt() == null ? "N/A" : String.valueOf(invoice.getHeightNetWt())));
            form.setField("totalQuantity", String.valueOf(invoice.getTotalQuantity()) + " Brass");
            form.setField("vehicleNo", invoice.getDriver().getVehicleNo());
            form.setField("driverName", invoice.getDriver().getDriverName());
            form.setField("mobileNo", invoice.getDriver().getMobileNo());
            form.setField("outTime", invoice.getOutTime().format(DateTimeFormatter.ofPattern("hh:mm a")));
            stamper.setAnnotationFlattening(true);
            stamper.setFormFlattening(true);
        } catch (Exception e) {
            log.error("Exception while filling pdf template", e);
            throw new Exception("Exception while filling pdf template", e);
        } finally {
            stamper.close();
            pdfReader.close();
        }
        return baos.toByteArray();
    }
}
