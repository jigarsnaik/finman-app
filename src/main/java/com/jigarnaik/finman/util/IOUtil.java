package com.jigarnaik.finman.util;

import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;

public class IOUtil {

    private IOUtil() {}

    public byte[] toByteArray(InputStream inputStream) throws IOException {
        return inputStream.readAllBytes();
    }
}
