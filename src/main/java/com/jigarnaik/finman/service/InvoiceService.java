package com.jigarnaik.finman.service;

import com.jigarnaik.finman.domain.Invoice;
import com.jigarnaik.finman.repository.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Log4j2
public class InvoiceService {

    private final InvoiceRepository invoiceRepository;
    private final CustomerRepository customerRepository;
    private final AddressRepository addressRepository;
    private final ProductRepository productRepository;
    private final DriverRepository driverRepository;

    public InvoiceService(InvoiceRepository invoiceRepository, CustomerRepository customerRepository, AddressRepository addressRepository, ProductRepository productRepository, DriverRepository driverRepository) {
        this.invoiceRepository = invoiceRepository;
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
        this.productRepository = productRepository;
        this.driverRepository = driverRepository;
    }

    public String getNextInvoiceNo() {
        return invoiceRepository.nextInvoiceNo();
    }

    public Invoice saveOrUpdate(Invoice invoice) {
        var customer = customerRepository.findById(invoice.getCustomer().getId()).orElseThrow(() -> new RuntimeException("Customer not found exception."));
        var address = addressRepository.findById(invoice.getAddress().getId()).orElseThrow(() -> new RuntimeException("Address not found exception."));
        var product = productRepository.findById(invoice.getProduct().getId()).orElseThrow(() -> new RuntimeException("Product not found exception."));
        var driver = driverRepository.findById(invoice.getDriver().getId()).orElseThrow(() -> new RuntimeException("Driver not found exception."));
        invoice.setCustomer(customer);
        invoice.setAddress(address);
        invoice.setDriver(driver);
        invoice.setProduct(product);
        return invoiceRepository.save(invoice);
    }


    public Page<Invoice> findInvoices(String royaltyNo, String invoiceNo, String customerName, LocalDate purchaseDateFrom, LocalDate purchaseDateTo, PageRequest pageRequest) {

        List<Predicate> predicates = new ArrayList<>();
        Specification<Invoice> specification = (root, query, cb) -> {
            Path<Object> customer = root.get("customer");
            predicates.add(cb.equal(root.get("active"), Boolean.TRUE));
            if (!Objects.isNull(royaltyNo)) predicates.add(cb.equal(root.get("royaltyNo"), royaltyNo));
            if (!Objects.isNull(invoiceNo)) predicates.add(cb.equal(root.get("invoiceNo"), invoiceNo));
            if (!Objects.isNull(customerName)) predicates.add(cb.like(cb.lower(customer.get("customerName")), "%" + customerName.toLowerCase() + "%"));
            if (!Objects.isNull(purchaseDateFrom)) predicates.add(cb.greaterThanOrEqualTo(root.get("purchaseDate"), purchaseDateFrom));
            if (!Objects.isNull(purchaseDateTo)) predicates.add(cb.lessThanOrEqualTo(root.get("purchaseDate"), purchaseDateTo));

            return cb.and(predicates.toArray(new Predicate[0]));
        };
        return invoiceRepository.findAll(specification, pageRequest);
    }

    public Invoice findById(Long id) {
        return invoiceRepository.findById(id).orElseThrow(() -> new RuntimeException("Invoice not found"));
    }
}
