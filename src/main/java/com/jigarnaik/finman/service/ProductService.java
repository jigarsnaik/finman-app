package com.jigarnaik.finman.service;

import com.jigarnaik.finman.domain.Product;
import com.jigarnaik.finman.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product saveOrUpdate(Product product) {
        return productRepository.save(product);
    }

    public List<Product> findAllActive() {
        return productRepository.findAllByActiveOrderByProductName(Boolean.TRUE);
    }


}
