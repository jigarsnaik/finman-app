package com.jigarnaik.finman.service;

import com.jigarnaik.finman.domain.Driver;
import com.jigarnaik.finman.repository.DriverRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DriverService {

    private final DriverRepository driverRepository;

    public DriverService(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    public Driver saveOrUpdate(Driver driver) {
        return driverRepository.save(driver);
    }

    public List<Driver> findAllActive() {
        return driverRepository.findAllByActiveOrderByDriverName(Boolean.TRUE);
    }
}
