package com.jigarnaik.finman.service;

import com.jigarnaik.finman.domain.Address;
import com.jigarnaik.finman.domain.Customer;
import com.jigarnaik.finman.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer saveOrUpdate(Customer customer) {
        for (Address address : customer.getAddresses()) {
            address.setCustomer(customer);
        }
        return customerRepository.save(customer);
    }

    public List<Customer> findAllActiveCustomers() {
        return customerRepository.findByActiveOrderByCustomerNameAsc(Boolean.TRUE);
    }

    public Customer findById(Long id) throws Exception {
        return customerRepository.findById(id).orElseThrow(() -> new Exception("Customer not found."));
    }

    public List<Customer> findByCustomerName(String customerName) {
        return customerRepository.findByCustomerNameAndActive(customerName, Boolean.TRUE);
    }

}
