package com.jigarnaik.finman.service;

import com.jigarnaik.finman.domain.Address;
import com.jigarnaik.finman.domain.Customer;
import com.jigarnaik.finman.repository.AddressRepository;
import com.jigarnaik.finman.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AddressService {

    private final AddressRepository addressRepository;
    private final CustomerRepository customerRepository;

    public AddressService(AddressRepository addressRepository, CustomerRepository customerRepository) {
        this.addressRepository = addressRepository;
        this.customerRepository = customerRepository;
    }

    public List<Address> findByCustomer(Long customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new RuntimeException("Customer not found"));
        return addressRepository.findByCustomer(customer);
    }
}
