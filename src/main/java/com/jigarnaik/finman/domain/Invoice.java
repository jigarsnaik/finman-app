package com.jigarnaik.finman.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "INVOICE")
@Getter
@Setter
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String invoiceNo;
    private LocalDate purchaseDate;
    private String royaltyNo;
    @OneToOne(targetEntity = Customer.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Customer customer;
    @OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;
    @OneToOne(targetEntity = Product.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Product product;
    private Double lengthGrossWt;
    private Double widthTareWt;
    private Double heightNetWt;
    private Double totalQuantity;
    @OneToOne(targetEntity = Driver.class, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Driver driver;
    private LocalTime outTime;
    private boolean active;

}
