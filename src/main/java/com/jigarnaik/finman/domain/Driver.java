package com.jigarnaik.finman.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "DRIVER")
@Getter
@Setter
public class Driver {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String driverName;
    private String vehicleNo;
    private String mobileNo;
    private String licenseNo;
    private boolean active;
}
